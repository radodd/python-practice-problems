# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    largest_value = 0
    for n in values:
        if n > largest_value:
            largest_value = n
    return largest_value


values = ([1,2,100, 101])
print(max_in_list(values))
