# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    if is_workday == True and is_sunny == False:
        return "Add umbrella to list"
    elif is_workday == True:
        return "Add laptop to list"
    else:
        return "Add surfboard to list"

is_workday = False
is_sunny = True
print(gear_for_day(is_workday, is_sunny))
